from rest_framework import serializers
from simplechat.models import User, Message

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['content', 'id', 'user_id']

class UserSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = '__all__'
