from rest_framework.viewsets import ModelViewSet
from simplechat.serializers import UserSerializer, MessageSerializer
from simplechat.models import User, Message

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class MessageViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
