from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.generics import CreateAPIView
from simplechat.serializers import MessageSerializer, UserSerializer
from simplechat.models import Message, User
from django.forms.models import model_to_dict
from rest_framework.response import Response
from rest_framework import status
from django.core.mail import send_mail

class ListMessagesAPIView(ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class CreateMessageAPIView(CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class CreateUserAPIView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

@api_view(['POST'])
def login(request):
    loginDetails = request.data
    user = User.objects.filter(email=loginDetails['email'], password=loginDetails['password']).first()
    if user is not None:
        user.isOnline = True
        user.save()
        return Response(data=model_to_dict(user))
    else:
        return Response(None, status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
def logout(request):
    user_id = request.data['user_id']
    user = User.objects.get(id=user_id)
    user.isOnline = False
    user.save()
    return Response(None, status=status.HTTP_200_OK)

@api_view(['POST'])
def addMessage(request):
    message = Message(content=request.data['content'], user_id=request.data['user_id'])
    message.save()
    return Response(None, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def setTypingStatus(request):
    user_id = request.data['user_id']
    user = User.objects.get(id=user_id)
    user.isTyping = request.data['is_typing']
    user.save()
    return Response(None, status=status.HTTP_200_OK)


@api_view(['GET'])
def getTypingUsers(request):
    typingUsers = []

    for user in User.objects.filter(isTyping=True):
        typingUsers.append([user.name])

    return Response(data=typingUsers, status=status.HTTP_200_OK)

@api_view(['POST'])
def sendMailsToOfflineUsers(request):
    offlineUsers = list(User.objects.filter(isOnline=False))

    for user in offlineUsers:
        send_mail(
            'You have a new message',
            'You have unread message waiting for you in the chat app. Open it to view your message!',
            'aso.test.30644@gmail.com',
            recipient_list=[user.email],
            fail_silently=False,
        )

    return Response(None, status=status.HTTP_200_OK)




