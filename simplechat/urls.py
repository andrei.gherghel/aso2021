from django.urls import path
from simplechat import views

urlpatterns = [
    path("messages", views.ListMessagesAPIView.as_view()),
    path("addMessage", views.addMessage),
    path("addUser", views.CreateUserAPIView.as_view()),
    path("login", views.login),
    path("logout", views.logout),
    path("setTypingStatus", views.setTypingStatus),
    path("getTypingUsers", views.getTypingUsers),
    path("sendMailsToOfflineUsers", views.sendMailsToOfflineUsers)
]
