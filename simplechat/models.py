from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    sessionId = models.UUIDField(null=True)
    isTyping = models.BooleanField(default=False)
    isOnline = models.BooleanField(default=False)

class Message(models.Model):
    content = models.TextField(blank=True)
    user = models.ForeignKey(User, related_name="messages", on_delete=models.CASCADE)
